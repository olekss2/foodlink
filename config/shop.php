<?php

return [

    'apc_enabled' => false, // enable for maximum performance if APCu is availalbe
    'apc_prefix' => 'aimeos:', // prefix for caching config and translation in APCu
    'pcntl_max' => 4, // maximum number of parallel command line processes when starting jobs

    'routes' => array(
        'about' => array('prefix' => '{site}/{locale}/{currency}'),
        'homepage' => array('prefix' => '{site}/{locale}/{currency}'),
        'products' => array('prefix' => '{site}/{locale}/{currency}'),
        'allShops' => array('prefix' => '{site}/{locale}/{currency}'),
        'categories' => array('prefix' => '{site}/{locale}/{currency}'),
        'changestocks' => array('prefix' => '{site}/{locale}/{currency}'),
        'changeprices' => array('prefix' => '{site}/{locale}/{currency}'),
        'usagepolitics' => array('prefix' => '{site}/{locale}/{currency}'),
        'politics' => array('prefix' => '{site}/{locale}/{currency}')
    ),


    'page' => [
        // Docs: https://aimeos.org/docs/Laravel/Adapt_pages
        // Hint: catalog/filter is also available as single 'catalog/tree', 'catalog/search', 'catalog/attribute'
        'account-index' => ['account/profile', 'account/review', 'account/subscription', 'account/history', 'account/favorite', 'account/watch', 'basket/mini', 'catalog/session', 'locale/select'],
        'basket-index' => ['basket/bulk', 'basket/standard', 'basket/related'],
        'catalog-count' => ['catalog/count'],
        'catalog-detail' => ['basket/mini', 'catalog/stage', 'catalog/detail', 'catalog/session', 'locale/select'],
        'catalog-home' => ['basket/mini', 'catalog/home', 'locale/select'],
        'catalog-list' => ['basket/mini', 'catalog/filter', 'catalog/lists', 'locale/select'],
        'catalog-stock' => ['catalog/stock'],
        'catalog-suggest' => ['catalog/suggest'],
        'catalog-tree' => ['basket/mini', 'catalog/filter', 'catalog/stage', 'catalog/lists', 'locale/select'],
        'checkout-confirm' => ['checkout/confirm'],
        'checkout-index' => ['checkout/standard'],
        'checkout-update' => ['checkout/update'],
        'homepage' => ['basket/mini', 'homepage', 'locale/select'],
        'about' => ['basket/mini', 'about', 'locale/select'],
        'contacts' => ['basket/mini', 'contacts', 'locale/select'],
        'allShops' => ['basket/mini', 'allShops', 'locale/select'],
        'categories' => ['basket/mini', 'categories', 'locale/select'],
        'changestocks' => ['basket/mini', 'changestocks', 'locale/select'],
        'changeprices' => ['basket/mini', 'changeprices', 'locale/select'],
        'ordertemplates' => ['basket/mini', 'ordertemplates', 'locale/select'],
        'debtLimits' => ['basket/mini', 'debtLimits', 'locale/select'],
        'usagepolitics' => ['basket/mini', 'usagepolitics', 'locale/select'],
        'politics' => ['basket/mini', 'politics', 'locale/select']
    ],


    /*
    'resource' => [
        'db' => [
            'adapter' => config('database.connections.mysql.driver', 'mysql'),
            'host' => config('database.connections.mysql.host', '127.0.0.1'),
            'port' => config('database.connections.mysql.port', '3306'),
            'socket' => config('database.connections.mysql.unix_socket', ''),
            'database' => config('database.connections.mysql.database', 'forge'),
            'username' => config('database.connections.mysql.username', 'forge'),
            'password' => config('database.connections.mysql.password', ''),
            'stmt' => ["SET SESSION sort_buffer_size=2097144; SET NAMES 'utf8mb4'; SET SESSION sql_mode='ANSI'"],
            'limit' => 3, // maximum number of concurrent database connections
            'defaultTableOptions' => [
                    'charset' => config('database.connections.mysql.charset'),
                    'collate' => config('database.connections.mysql.collation'),
            ],
        ],
    ],
    */

    'admin' => [],

    'client' => [
        'html' => [
            'email' => [
                'from-email' => 'info@foodlink.lv',
                'from-name' => 'Foodlink',
                'delivery' => [
                    'from-email' => 'info@foodlink.lv',
                    'from-name' => 'Foodlink'
                ]
            ],
            'basket' => [
                'cache' => [
                    ///// 'enable' => false, // Disable basket content caching for development
                ],
            ],
            'common' => [
                'template' => [
                    // 'baseurl' => public_path( 'packages/aimeos/shop/themes/elegance' ),
                ],
            ],
            'catalog' => [
                'selection' => [
                    'type' => [
                        'color' => 'radio',
                        'length' => 'radio',
                        'width' => 'radio',
                    ],
                ],
            ],
            'locale' => [
                'select' => [
                    'currency' => [
                        'param=name' => 'currency'
                    ],
                    'language' => [
                        'param=name' => 'locale'
                    ]
                ]
            ]
        ],
    ],

    'driver' => env('MAIL_DRIVER', 'smtp'),

    'host' => env('MAIL_HOST', 'arturso.sandbox@gmail.com'), // smtp


    'port' => env('MAIL_PORT', '465'), //smtp port


    'from' => [
        'address' => env('MAIL_FROM_ADDRESS', 'arturso.sandbox@gmail.com'),
        'name' => env('MAIL_FROM_NAME', 'Foodlink'),
    ],


    'encryption' => env('MAIL_ENCRYPTION', 'SSL'),


    'username' => env('valters.slava@smartekbaltic.lv'),

    'password' => env('Robotika123'),


    'markdown' => [
        'theme' => 'default',

        'paths' => [
            resource_path('views/vendor/mail'),
        ],
    ],


    'log_channel' => env('MAIL_LOG_CHANNEL'),

    /// ajouté
    'stream' => [
        'ssl' => [
            'allow_self_signed' => true,
            'verify_peer' => false,
            'verify_peer_name' => false,
        ],
    ],

    'controller' => [
        'jobs' => [
            'product' => [
                'import' => [
                    'csv' => [
                        'skip-lines' => 1,
                        'location' => 'loadFilesCSV',
                        'container' => [
                            'type' => 'Directory',
                            'content' => 'CSV'
                        ],
                        'mapping' => [
                            'item' => [
                                0 => 'product.code', // e.g. unique EAN code
                                1 => 'product.label', // UTF-8 encoded text, also used as product name
                                2 => 'product.type', // type of the product, e.g. "default" or "selection"
                                3 => 'product.status', // enabled (1) or disabled (0)
                            ],
                            'text' => [
                                4 => 'text.type', // e.g. "short" for short description
                                5 => 'text.content', // UTF-8 encoded text
                                6 => 'text.type', // e.g. "long" for long description
                                7 => 'text.content', // UTF-8 encoded text
                            ],
                            'media' => [
                                8 => 'media.url', // relative URL of the product image on the server
                            ],
                            'price' => [
                                9 => 'price.currencyid', // three letter ISO currency code
                                10 => 'price.quantity', // the quantity the price (for block pricing)
                                11 => 'price.value', // price with decimals separated by a dot
                                12 => 'price.taxrate', // tax rate with decimals separated by a dot
                            ],
                            'catalog' => [
                                13 => 'catalog.code', // e.g. Unique category code
                            ],
                            'stock' => [
                                14 => 'stock.stocklevel'
                            ],
                            'supplier' => [
                                15 => 'supplier.code'
                            ],
                            'attribute' => [
                                16 => 'attribute.code', // code of an attribute, will be created if not exists
                                17 => 'attribute.type', // e.g. "size", "length", "width", "color", etc.
                                18 => 'attribute.code', // code of an attribute, will be created if not exists
                                19 => 'attribute.type', // e.g. "size", "length", "width", "color", etc.
                                20 => 'attribute.code', // code of an attribute, will be created if not exists
                                21 => 'attribute.type', // e.g. "size", "length", "width", "color", etc.
                                22 => 'attribute.code', // code of an attribute, will be created if not exists
                                23 => 'attribute.type', // e.g. "size", "length", "width", "color", etc.
                                24 => 'attribute.code', // code of an attribute, will be created if not exists
                                25 => 'attribute.type', // e.g. "size", "length", "width", "color", etc.
                            ]
                        ]
                    ]
                ]
            ],
            "supplier" => [
                "import" => [
                    "csv" => [
                        'skip-lines' => 1,
                        'location' => 'loadFilesCSV',
                        'container' => [
                            'type' => 'Directory',
                            'content' => 'CSV'
                        ],
                        "mapping" => [
                            "item" => [
                                0 => "supplier.code",
                                1 => "supplier.label"
                            ],
                            "address" => [
                                2 => "supplier.address.languageid",
                                3 => "supplier.address.countryid",
                                4 => "supplier.address.city",
                                5 => "supplier.address.company",
                                6 => "supplier.address.email",
                                7 => "supplier.address.telephone"
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'frontend' => [
            'basket' => [
                'limit-seconds' => 1,
            ]
        ]
    ],

    'i18n' => [
    ],

    'madmin' => [
        'cache' => [
            'manager' => [
                // 'name' => 'None', // Disable caching for development
            ],
        ],
        'log' => [
            'manager' => [
                'standard' => [
                    // 'loglevel' => 7, // Enable debug logging into madmin_log table
                ],
            ],
        ],
    ],

    'mshop' => [
        'price' => [
            'precision' => 4
        ]
    ],


    'command' => [
    ],

    'frontend' => [
    ],

    'backend' => [
    ]

];
