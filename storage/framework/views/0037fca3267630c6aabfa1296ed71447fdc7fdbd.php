<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Foodlink</title>
    <link rel="shortcut icon" type="image/png" href="/files/logoSmall.png"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/combine/npm/flatpickr@4/dist/flatpickr.min.css,npm/flatpickr@4/dist/plugins/confirmDate/confirmDate.min.css">
    <link rel="stylesheet"
          href="<?= route('clientAdmin_file', array('site' => $site, 'lang' => 'en', 'type' => 'css')); ?>"/>
</head>
<body>
<div class="app-menu" style="background: #202830; text-align: right">
</div>

<?= $content ?>
</body>
</html>
<?php /**PATH C:\xampp\htdocs\myshop\resources\views/clientAdminIndex.blade.php ENDPATH**/ ?>