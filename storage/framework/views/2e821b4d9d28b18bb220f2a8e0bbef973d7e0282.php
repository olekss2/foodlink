<?php $__env->startSection('aimeos_styles'); ?>
	<link type="text/css" rel="stylesheet" href="<?php echo e(asset(config( 'shop.client.html.common.template.baseurl', 'packages/aimeos/shop/themes/elegance' ) . '/aimeos.css')); ?>" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('aimeos_scripts'); ?>
	<script type="text/javascript" src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
	<script type="text/javascript" src="<?php echo e(asset('packages/aimeos/shop/themes/aimeos.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(asset(config( 'shop.client.html.common.template.baseurl', 'packages/aimeos/shop/themes/elegance' ) . '/aimeos.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\myshop\vendor\aimeos\aimeos-laravel\src\views/base.blade.php ENDPATH**/ ?>