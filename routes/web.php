<?php

Auth::routes();

Route::get('/admin_client', 'AdminClientController@indexAction');
Route::match(array('GET', 'POST'), 'search/{resource}', array(
    'as' => 'admin_client_search',
    'uses' => 'AdminClientController@searchAction'
))->where(['site' => '[a-z0-9\.\-]+']);

Route::match(array('GET'), 'file/{type}', array(
    'as' => 'clientAdmin_file',
    'uses' => 'AdminClientController@fileAction'
))->where(['site' => '[a-z0-9\.\-]+']);

Route::match(array('GET'), 'adminCient/get/{resource}/{id}', array(
    'as' => 'admin_client_get',
    'uses' => 'AdminClientController@getAction'
))->where(['site' => '[a-z0-9\.\-]+'])->where(array('resource' => '[a-z\/]+'));

Route::get('/products', '\Aimeos\Shop\Controller\CatalogController@listAction')->name('aimeos_home');
Route::get('/shops', 'AllShopsController@index')->name('aimeos_home');
Route::get('/categories', 'CategoriesController@index')->name('aimeos_home');
Route::get('/about', 'AboutController@homeAction')->name('aimeos_home');
Route::get('/usagepolitics', 'UsagePoliticsController@homeAction')->name('aimeos_home');
Route::get('/politics', 'PoliticsController@homeAction')->name('aimeos_home');
Route::get('/contacts', 'ContactsController@homeAction')->name('aimeos_home');
Route::get('/changeprices', 'ChangePricesController@index')->name('aimeos_home');
Route::get('/changestocks', 'ChangeStocksController@index')->name('aimeos_home');
Route::get('/orderTemplates', 'OrderTemplatesController@index')->name('aimeos_home');
Route::get('/debtLimits', 'DebtLimitsController@index')->name('aimeos_home');
Route::get('/', 'HomePageController@homeAction')->name('aimeos_home');
Route::get('/{lang}', function ($lang) {
    App::setlocale($lang);
    return redirect('/');
});

Route::post('/changestocks', 'ChangeStocksController@index')->name('aimeos_home');
Route::post('/changeprices', 'ChangePricesController@updateDiscounts')->name('aimeos_home');
Route::post('/orderTemplates', 'OrderTemplatesController@index')->name('aimeos_home');
Route::post('/debtLimits', 'DebtLimits@index')->name('aimeos_home');
