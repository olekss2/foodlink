<?php

$supplier = false;
$customer = false;
if (!Auth::guest()) {
    $rolesDB = DB::table("users_list")->
    select("refid")->
    where('parentid', "=", Auth::user()->id)->where('domain', '=', 'customer/group')->get()->all();;

    foreach ($rolesDB as $roleID) {

        switch ($roleID->refid) {
            case "3":
                $customer = true;
                break;
            case "4":
                $supplier = true;
                break;
            case "1":
                $supplier = true;
                $customer = true;
                break;
        }
    }
}
?>
    <!DOCTYPE html>
<html class="no-js">

<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <!-- ///content="IE=edge"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,400italic,300italic' rel='stylesheet'
          type='text/css'>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet" type='text/css'>

    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css?v=').time()}}">
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/icomoon.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('owlcarousel/owlcarousel/assets/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/magnific-popup.css') }}">

    @yield('aimeos_styles')

    @yield('aimeos_header')
    <title>FoodLink</title>
    <link rel="shortcut icon" type="image/png" href="/files/logoSmall.png"/>
</head>

<body onload="hideNav()">
<div class="container pt-5">
    <div class="row justify-content-between">
        <a class="navbar-brand" href="/">
            <img src="/files/logo.png" height="90" title="FoodLink">
        </a>

        <?php if(false) :?>
        <div class="col d-flex justify-content-end topContent">
            <ul class="navbar-nav">
                @if (Auth::guest())
                    <li class="nav-item"><a class="nav-link" href="/search/newOrder">{{ __('mainElements.login')}}</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="/register">{{ __('mainElements.register')}}</a></li>
                @else
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a class="nav-link"
                                   href=<?= route('aimeos_shop_account', ['site' => Route::current()->parameter('site', 'default'),
                                       'locale' => Route::current()->parameter('locale', app()->getLocale()), 'EUR']) ?> title="Profile">Profile</a>
                            </li>
                            <li>
                                <form id="logout" action="/logout" method="POST">{{csrf_field()}}</form>
                                <a class="nav-link"
                                   href="javascript: document.getElementById('logout').submit();">{{ __('mainElements.logout')}}</a>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
            @yield('aimeos_head')
        </div>
        <?php endif; ?>
    </div>
</div>
<?php if(false): ?>
<button style="min-width: 100%" class="ftco-navbar-light dropbtn choiceSection" onclick="choiceBtn()">
    <img src="/files/icons/choice_white.png"
         height="25"/> {{ __('mainElements.choice')}}
</button>
<?php endif;?>
<?php if(false) :?>
<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item"><a href="{{ url('/') }}" class="nav-link"><img src="/files/icons/home_white.png"
                                                                                    height="25"> {{ __('mainElements.home')}}
                    </a></li>
                <div class="dropdown">
                    <li class="nav-item">
                        <button class="dropbtn" onclick="closeDropdown('catalogContent')">
                            <img
                                src="/files/icons/shop_white.png" height="25">
                            {{ __('mainElements.shop')}}
                        </button>
                    </li>
                    <div class="dropdown-content" id="catalogContent">
                        <li class="nav-item">
                            <a href="{{ url('/shops') }}" class="nav-link" style="color: white">
                                <img
                                    src="/files/icons/shops_white.png" height="25"> {{ __('mainElements.allShops')}}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/categories') }}" class="nav-link" style="color: white"><img
                                    src="/files/icons/categories_white.png"
                                    height="25"> {{ __('mainElements.allCategories')}}
                            </a>
                        </li>
                        <li class="nav-item"><a href="{{ url('/products') }}" class="nav-link"
                                                style="color: white"><img
                                    src="/files/icons/products_white.png"
                                    height="25"> {{ __('mainElements.allProducts')}}
                            </a>
                        </li>
                    </div>
                </div>


                @if ($supplier)

                    <div class="dropdown">
                        <li class="nav-item">
                            <button class="dropbtn" onclick="closeDropdown('suppliers')">
                                <img
                                    src="/files/icons/suppliers_icon.png" height="25">
                                {{ __('mainElements.forSuppliers')}}
                            </button>
                        </li>
                        <div class="dropdown-content" id="suppliers">
                            <li class="nav-item">
                                <a href="{{ url('/changestocks') }}" class="nav-link" style="color: white">
                                    <img
                                        src="/files/icons/stock.png" height="25"> {{ __('mainElements.changestocks')}}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('/changeprices') }}" class="nav-link" style="color: white"><img
                                        src="/files/icons/price.png" height="25"> {{ __('mainElements.changeprices')}}
                                </a>
                            </li>
                            <li class="nav-item"><a href="{{ url('/debtLimits') }}" class="nav-link"
                                                    style="color: white">
                                    <img
                                        src="/files/icons/debt_limits.png"
                                        height="25"> {{ __('mainElements.debtLimits')}}
                                </a>
                            </li>
                        </div>
                    </div>
                @endif
                @if ($customer)
                    <li class="nav-item"><a href="{{ url('/orderTemplates') }}" class="nav-link">
                            <img
                                src="/files/icons/order_template.png"
                                height="25"> {{ __('mainElements.orderTemplates')}}
                        </a></li>
                @endif
                <li class="nav-item"><a href="{{ url('/about') }}" class="nav-link">
                        <img
                            src="/files/icons/about_white.png" height="25"> {{ __('mainElements.about')}}</a></li>
                <li class="nav-item"><a href="{{ url('/contacts') }}" class="nav-link">
                        <img
                            src="/files/icons/contacts_icon.png" height="25"> {{ __('mainElements.contacts')}}</a></li>
            </ul>
        </div>
    </div>
</nav>
<?php endif;?>

<div class="content">
    @yield('aimeos_stage')
    @yield('aimeos_nav')
    @yield('aimeos_body')
    @yield('aimeos_aside')
    @yield('content')
</div>
<footer class="mt-5 p-5">
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-sm-6 my-4">
                    <h2>{{ __('mainElements.aboutFoodlink')}}</h2>
                    <p><a href="{{ url('/contacts') }}">{{ __('mainElements.contacts')}}</a></p>
                    <p><a href="{{ url('/about') }}">{{ __('mainElements.about')}}</a></p>
                    <p><a href="{{ url('/usagepolitics') }}">{{ __('mainElements.usagepolitics')}}</a></p>
                    <p><a href="{{ url('/politics') }}">{{ __('mainElements.politics')}}</a></p>
                </div>
                <div class="col-3 my-4">
                    <p><i class="fa fa-envelope"></i> {{ __('mainElements.e_mail')}}:
                        <br><b>valters.slava@foodlink.lv</b></p>
                    <p><i class="fa fa-phone"></i> {{ __('mainElements.phoneNo')}}: <br><b>+371 28 323 341</b></p>
                    <p><a href="https://www.facebook.com/FoodLinkLatvia" class="sm facebook" title="Facebook"
                          rel="noopener">
                            <span class="fa fa-facebook"><i class="sr-only"></i></span></a></p>
                </div>
            </div>
        </div>
        <div class="col-md-4 my-4">
            <a class="px-2 py-4 d-inline-block" href="/">
                <img src="/files/logo.png" style="width: 160px"
                     title="">
            </a>
        </div>
    </div>
</footer>
<!-- Scripts -->
<script src="{{ URL::asset('js/popper.min.js') }}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/combine/npm/jquery@3,npm/bootstrap@4"></script>
@yield('aimeos_scripts')
<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.waypoints.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.stellar.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.animateNumber.min.js') }}"></script>
<script src="{{ URL::asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ URL::asset('js/scrollax.min.js') }}"></script>
<script src="{{ URL::asset('js/main.js') }}"></script>
<script src="{{ URL::asset('js/popupVideo.js') }}"></script>

<style>
    /* Dropdown Button */
    .dropbtn {
        background-color: transparent;
        color: white;
        padding: 16px;
        padding-top: 1.8rem;
        padding-bottom: 1.8rem;
        padding-left: 30px;
        padding-right: 30px;
        font-weight: 500;
        font-size: 14px;
        position: relative;
        opacity: 1 !important;
        border: none;
    }

    /* The container <div> - needed to position the dropdown content */
    .dropdown {
        position: relative;
        display: inline-block;
    }


    @media (min-width: 44em) {
        /* Dropdown Content (Hidden by Default) */
        .dropdown-content {
            display: none;
            position: absolute;
            font-size: 14px;
            background-color: #00043c !important;
            color: white;
        !important;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
        }

        /* Links inside the dropdown */
        .dropdown-content a {
            color: white;
        !important;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }
    }

    @media (max-width: 44em) {
        /* Dropdown Content (Hidden by Default) */
        .dropdown-content {
            display: none;
            position: relative;
            font-size: 14px;
            background-color: #00043c !important;
            color: white;
        !important;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
        }

        /* Links inside the dropdown */
        .dropdown-content a {
            color: white;
        !important;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
            position: relative;
        }
    }

    /* Change color of dropdown links on hover */
    .dropdown-content a:hover {
        background-color: #1c7430;
        color: white;
    !important;
    }

    /* Show the dropdown menu on hover */
    .dropdown:hover .dropdown-content {
        display: block;
        color: white;
    !important;
    }

    /* Change the background color of the dropdown button when the dropdown content is shown */
    .dropdown:hover .dropbtn {
        color: white;
    !important;
    }
</style>
<script>
    function closeDropdown(id) {
        var x = window.matchMedia("(max-width: 44em)")
        if (!x.matches)
            return;

        var element = document.getElementById(id);

        if (element.style.display == "block")
            element.style.display = "none";
        else if (element.style.display == "none")
            element.style.display = "block";
        else
            element.style.display = "block";

    }

    function choiceBtn() {
        var element = document.getElementById("ftco-navbar");

        if (element.style.display != "none")
            element.style.display = "none";
        else
            element.style.removeProperty("display");
    }

    function hideNav() {
        var x = window.matchMedia("(max-width: 44em)")
        if (!x.matches)
            return;

        var element = document.getElementById("ftco-navbar");

        if (element.style.display != "none")
            element.style.display = "none";
        else
            element.style.removeProperty("display");
    }

</script>
</body>

</html>
