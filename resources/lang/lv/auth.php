<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Nepareizi ievadīts E-pasts vai/un Parole',
    'throttle' => 'Pārak daudz pierakstīšanās mēģinājumu. Lūdzu mēģinēt vēlreiz pēc :seconds sekundēm.',
    'Login' => 'Lietotājs',
    'E-Mail Address' => 'E-pasts',
    'Password' => 'Parole',
    'Remember Me' => 'Atcerēties mani',
    'Log in' => 'Ierakstīties',
    'Forgot Your Password?' => 'Aizmirsāt paroli?'


];
