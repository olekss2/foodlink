<?php

return [
    'home' => 'Home',
    'products' => 'Products',
    'about' => 'About',
    'changestocks' => 'Change stocks',
    'changeprices' => "Change prices"
];
