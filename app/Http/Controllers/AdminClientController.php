<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;


/**
 * Aimeos controller for the JQuery admin interface
 *
 * @package laravel
 * @subpackage Controller
 */
class AdminClientController extends Controller
{
    use AuthorizesRequests;


    /**
     * Returns the JS file content
     *
     * @return \Illuminate\Http\Response Response object containing the generated output
     */
    public function fileAction()
    {

        $contents = '';
        $files = array();
        $aimeos = app('aimeos')->get();
        $type = Route::input('type', Request::get('type', 'js'));

        foreach ($aimeos->getCustomPaths('admin/clientAdmin') as $base => $paths) {
            foreach ($paths as $path) {
                $jsbAbsPath = $base . '/' . $path;
                $jsb2 = new \Aimeos\MW\Jsb2\Standard($jsbAbsPath, dirname($jsbAbsPath));
                $files = array_merge($files, $jsb2->getFiles($type));
            }
        }

        foreach ($files as $file) {
            if (($content = file_get_contents($file)) !== false) {
                $contents .= $content;
            }
        }

        $response = response($contents);

        if ($type === 'js') {
            $response->header('Content-Type', 'application/javascript');
        } elseif ($type === 'css') {
            $response->header('Content-Type', 'text/css');
        }

        return $response;
    }


    /**
     * Returns the HTML code for a copy of a resource object
     *
     * @return string Generated output
     */
    public function copyAction()
    {
        $cntl = $this->createAdmin();

        if (($html = $cntl->copy()) == '') {
            return $cntl->getView()->response();
        }

        return $this->getHtml($html);
    }


    /**
     * Returns the HTML code for a new resource object
     *
     * @return string Generated output
     */
    public function createAction()
    {
        $cntl = $this->createAdmin();

        if (($html = $cntl->create()) == '') {
            return $cntl->getView()->response();
        }

        return $this->getHtml($html);
    }


    /**
     * Deletes the resource object or a list of resource objects
     *
     * @return string Generated output
     */
    public function deleteAction()
    {
        $cntl = $this->createAdmin();

        if (($html = $cntl->delete()) == '') {
            return $cntl->getView()->response();
        }

        return $this->getHtml($html);
    }


    /**
     * Exports the data for a resource object
     *
     * @return string Generated output
     */
    public function exportAction()
    {
        $cntl = $this->createAdmin();

        if (($html = $cntl->export()) == '') {
            return $cntl->getView()->response();
        }

        return $this->getHtml($html);
    }


    /**
     * Returns the HTML code for the requested resource object
     *
     * @return string Generated output
     */
    public function getAction()
    {
        if (Auth::guest())
            return redirect()->guest('login');

        $cntl = $this->createAdmin();

        if (($html = $cntl->get()) == '') {
            return $cntl->getView()->response();
        }

        return $this->getHtml($html);
    }


    /**
     * Saves a new resource object
     *
     * @return string Generated output
     */
    public function saveAction()
    {
        $cntl = $this->createAdmin();

        if (($html = $cntl->save()) == '') {
            return $cntl->getView()->response();
        }

        return $this->getHtml($html);
    }


    public function indexAction(\Illuminate\Http\Request $request)
    {
        if (Auth::guest())
            return redirect()->guest('login');

        $context = app('aimeos.context')->get(false);
        $siteManager = \Aimeos\MShop::create($context, 'locale/site');
        $siteId = current(array_reverse(explode('.', trim($request->user()->siteid, '.'))));

        $siteCode = ($siteId ? $siteManager->getItem($siteId)->getCode() : 'default');
        $locale = $request->user()->langid ?: config('app.locale', 'en');

        $param = array(
            'resource' => 'dashboard',
            'site' => Route::input('site', Request::get('site', $siteCode)),
            'lang' => Route::input('lang', Request::get('lang', $locale))
        );

        return redirect()->route('admin_client_search', $param);
    }

    /**
     * Returns the HTML code for a list of resource objects
     *
     * @return string Generated output
     */
    public function searchAction(\Illuminate\Http\Request $request)
    {
        if (Auth::guest())
            return redirect()->guest('login');

        $suppliers = $request->request->get("f_supid", []);
        $currentQueries = $request->query();

        $diff = false;
        if ($suppliers != [] && $suppliers[0] == null && !array_key_exists("f_supid", $currentQueries))
            $diff = true;
        elseif (array_key_exists("f_supid", $currentQueries) && count($suppliers) > 0 && count($currentQueries["f_supid"]) > 0)
            $diff = $currentQueries["f_supid"][0] == $suppliers[0];
        elseif ($suppliers == [])
            $diff = true;

        if (!$diff):
            if (array_key_exists("page", $currentQueries) && array_key_exists("offset", $currentQueries["page"]))
                $currentQueries["page"]["offset"] = 0;
            $currentSuppliers = [];
            $setNew = true;
            $newQueries = [];
            foreach ($currentQueries as $id => $value):
                if (substr($id, 0, 7) != "f_supid")
                    $newQueries[$id] = $value;
            endforeach;

            if ($suppliers != [])
                $newQueries["f_supid"] = $suppliers;

            $queriesUrl = "";
            if ($newQueries != []) {
                $queriesUrl = "?";
                foreach ($newQueries as $id => $query):
                    if ($queriesUrl != "?")
                        $queriesUrl = $queriesUrl . "&";

                    if ($id != "f_supid"):
                        if (!(is_array($query)))
                            $queriesUrl = $queriesUrl . $id . "=" . $query;
                        else {
                            $elementNo = 0;
                            $lastElementNo = count($query);
                            foreach ($query as $key => $value):
                                $elementNo++;
                                $queriesUrl = $queriesUrl . $id . urlencode("[" . $key . "]") . "=" . $value;
                                if ($elementNo != $lastElementNo)
                                    $queriesUrl = $queriesUrl . "&";
                            endforeach;
                        }

                    else:
                        $index = 0;
                        foreach ($query as $supID):
                            if ($supID == "")
                                continue;
                            if ($queriesUrl != "?")
                                $queriesUrl = $queriesUrl . "&";
                            $queriesUrl = $queriesUrl . "f_supid" . urlencode("[" . $index . "]") . "=" . $supID;
                            $index++;
                        endforeach;
                    endif;
                endforeach;
            }
            return redirect($request->url() . "/" . $queriesUrl);
        endif;
        $cntl = $this->createAdmin();

        if (($html = $cntl->search()) == '') {
            return $cntl->getView()->response();
        }

        return $this->getHtml($html);
    }

    protected function createAdmin(): \Client\Admin\JQAdm\Iface
    {
        $site = Route::input('site', Request::get('site', 'default'));
        $lang = Request::get('lang', config('app.locale', 'en'));
        $resource = Route::input('resource');
        if ($resource == null)
            $resource = "dashboard";


        $aimeos = app('aimeos')->get();
        $paths = $aimeos->getCustomPaths('admin/clientAdmin/templates');

        $context = app('aimeos.context')->get(false, 'backend');
        $context->setI18n(app('aimeos.i18n')->get(array($lang, 'en')));
        $context->setLocale(app('aimeos.locale')->getBackend($context, $site));

        $view = app('aimeos.view')->create($context, $paths, $lang);

        $view->aimeosType = 'Laravel';
        $view->aimeosVersion = app('aimeos')->getVersion();
        $view->aimeosExtensions = implode(',', $aimeos->getExtensions());
        $context->setView($view);

        return \Client\Admin\JQAdm::create($context, $aimeos, $resource);
    }


    /**
     * Returns the generated HTML code
     *
     * @param string $content Content from admin client
     * @return \Illuminate\Contracts\View\View View for rendering the output
     */
    protected function getHtml(string $content)
    {
        $site = Route::input('site', Request::get('site', 'default'));
        return View::make('clientAdminIndex', array('content' => $content, 'site' => $site));
    }
}
