<?php


namespace App\Http\Controllers;

/**
 * @license MIT, http://opensource.org/licenses/MIT
 * @copyright Aimeos (aimeos.org), 2015-2016
 * @package laravel
 * @subpackage Controller
 */

use Aimeos\Shop\Facades\Shop;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;


/**
 * Aimeos controller for catalog related functionality.
 *
 * @package laravel
 * @subpackage Controller
 */
class AboutController extends Controller
{

    /**
     * Returns the home page.
     *
     * @return \Illuminate\Http\Response Response object with output and headers
     */
    public function homeAction(Request $request)
    {

        $lang = \App::getLocale();
        $currentQueries = $request->query();

        if (array_key_exists("locale", $currentQueries)  && !($currentQueries["locale"] === $lang)) {
            \App::setLocale($currentQueries["locale"]);
            session()->put('locale', $currentQueries["locale"]);
        }

        //SetLocale
        if (!array_key_exists("locale", $currentQueries)) {
            $newQueries = ["locale" => $lang];

            //Merge together current and new query strings:
            $allQueries = array_merge($currentQueries, $newQueries);

            //Generate the URL with all the queries:
            $request->fullUrlWithQuery($allQueries);
            return redirect($request->fullUrlWithQuery($allQueries));
        }


        foreach (app('config')->get('shop.page.about') as $name) {
            $params['aiheader'][$name] = Shop::get($name)->getHeader();
            $params['aibody'][$name] = Shop::get($name)->getBody();
        }

        return Response::view('about', $params)
            ->header('Cache-Control', 'private, max-age=10');
    }
}
