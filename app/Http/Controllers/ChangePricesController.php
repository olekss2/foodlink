<?php


namespace App\Http\Controllers;

/**
 * @license MIT, http://opensource.org/licenses/MIT
 * @copyright Aimeos (aimeos.org), 2015-2016
 * @package laravel
 * @subpackage Controller
 */

use Aimeos\Shop\Facades\Shop;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;


/**
 * Aimeos controller for catalog related functionality.
 *
 * @package laravel
 * @subpackage Controller
 */
class ChangePricesController extends Controller
{


    public function index(Request $request)
    {

        $lang = \App::getLocale();
        $currentQueries = $request->query();

        if (array_key_exists("locale", $currentQueries) && !($currentQueries["locale"] === $lang)) {
            \App::setLocale($currentQueries["locale"]);
            session()->put('locale', $currentQueries["locale"]);
        }

        //SetLocale
        if (!array_key_exists("locale", $currentQueries)) {
            $newQueries = ["locale" => $lang];

            //Merge together current and new query strings:
            $allQueries = array_merge($currentQueries, $newQueries);

            //Generate the URL with all the queries:
            $request->fullUrlWithQuery($allQueries);
            return redirect($request->fullUrlWithQuery($allQueries));
        }


        foreach (app('config')->get('shop.page.changeprices') as $name) {
            $params['aiheader'][$name] = Shop::get($name)->getHeader();
            $params['aibody'][$name] = Shop::get($name)->getBody();
        }

        return Response::view('changeprices', $params)
            ->header('Cache-Control', 'private, max-age=10');
    }

    public function updateDiscounts(Request $request)
    {
        $data = $request->input();

        $product = 0;
        $discounts = [];
        foreach ($data as $field => $value) {
            if ($field === "_token")
                continue;

            if ($field === "save")
                break;

            $current = explode("_", $field);

            if ($current[1] === "product") {
                $product = $value;
                $discounts = []; ////Keep only those discounts which are saved
            }

            if ($current[1] === "customer")
                $discounts[$current[2]] = $value;


        }

        foreach ($discounts as $customer => $discount) {
            DB::table('discounts')->where(['product_id' => $product, 'customer_id' => $customer])->
            update(['discount' => floatval($discount)]);
        }

        return $this->index($request);
    }

}
